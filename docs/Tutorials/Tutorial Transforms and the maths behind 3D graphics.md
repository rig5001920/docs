
# Local transform

Say we created a 3D model. For what its worth, the computer will represent it as an array of Vertices, which we will here assume to be a simple $\mathbb{R}^3$ vector. Each one of these points in space are described relative to the origin of the model. The origin of the model is comprised of three things: It's position, it's rotation and it's scale. Each one of these are relative to the object's parent, so on and so forth.

As such, It would be ideal for the computer's sake to combine all of theses transformations into a single transformation before rendering each and every vertex.

The idea is to note that both rotations and scalings are linear transformations, for example for scaling, we simply have:
$$
\begin{pmatrix}s_xx \\ s_yy \\ s_zz \end{pmatrix}=\begin{pmatrix} s_x & 0 & 0 \\ 0 & s_y & 0 \\ 0 & 0 & s_z \end{pmatrix}\begin{pmatrix}x \\ y \\ z \end{pmatrix}
$$
and for rotations around x, y and z axis, we have:
$$
R_{x, \theta}(X)=\begin{pmatrix}
1 & 0 & 0 \\
0 & \cos(\theta) & -\sin(\theta) \\
0 & \sin(\theta) & \cos(\theta)
\end{pmatrix}
$$
$$
R_{y, \theta}(X)=\begin{pmatrix}
\cos(\theta) & 0 & \sin(\theta) \\
0 & 1 & 0 \\
-\sin(\theta) & 0 & \cos(\theta)
\end{pmatrix}
$$
$$
R_{x, \theta}(X)=\begin{pmatrix}

\cos(\theta) & -\sin(\theta) & 0 \\
\sin(\theta) & \cos(\theta)  & 0 \\
0 & 0 & 1
\end{pmatrix}
$$
Note that a general formula is buildable, but a little more complex than these simple examples. This is great, however translations are  affine transforms, not a linear ones. As such, we cannot construct a 3x3 matrix to represent translations. We can, however, build a 4x4 matrix to emulate the behaviour of translation through the use of a shearing operation. Long story short, the following equality holds:

$$
\begin{pmatrix}
x+t_x \\
y+t_y \\
z+t_z \\
1
\end{pmatrix}
=
\begin{pmatrix}
1 & 0 & 0 & t_x \\
0 & 1 & 0 & t_y \\
0 & 0 & 1 & t_z \\
0 & 0 & 0 & 1
\end{pmatrix}
\begin{pmatrix}
x \\
y \\
z \\
1
\end{pmatrix}
$$
In addition to this, for any 3x3 matrix $M$ and any $X\in \mathbb{R^3}$
$$
\begin{pmatrix}
MX \\
1
\end{pmatrix}
=
\begin{pmatrix}
M & 0 \\
0 & 1
\end{pmatrix}
\begin{pmatrix}
X \\
1
\end{pmatrix}
$$
This means we can take all of our previous rotation and scaling matrices and convert them into a homologous 4x4 matrix. Once this is done, we can finally build our transform matrix as follows:

$$
T=O_TO_RO_S
$$
With $O_T$ the object's translation matrix, $O_R$ the object's rotation matrix and $O_S$ the object's scaling matrix.

# Global transform
Now assume we have an object $O_n$ who's parent is $O_{n-1}$, and so forth up until $O_0$. Each object has a local transform $T_k$ defined as above. We define $O_n$'s global transform $G_n$ as follows:

$$
G_n=\prod_{k=0}^nT_k
$$
This transform has baked into itself all of the local transforms of each object. As such, we can directly use it on each vertex without having to move up the complete stack for each vertex.

# Camera transform

This is great and all, but our end-goal is to have a 2D image of all of our objects relative to where the camera is facing. The next logical step is therefore to transform the space to counter the camera's own rotation and translation. Essentially, we move each and every object in the scene the opposite amount of what the camera is meant to move, to emulate the same effect. We denote this transform the Camera's transform, or $C$. If we denote $C_R$ the camera's rotation matrix and $C_T$ the camera's translation matrix, we would have:
$$
C=C_R^{-1}C_T^{-1}
$$
Remember: we want the opposite of the transforms the camera applies, so we need:
$$
C\times C_T C_R = I \iff C_R^{-1}C_T^{-1}C_TC_R=I \iff I=I
$$
# Projection
Once our positions are relative to the camera, we need to project our points onto a 2D grid. Good news: this is possible through the use of matrices provided our projection is linear. There are, however, different types of transforms, such as perspective projections and orthogonal projections. Their matrices are more or less irrelevant here.

# Conclusion

If we denote $P$ our projection matrix, we end up with the following formula for our local-to-viewport transform:

$$
M=PCG=PC_R^{-1}C_T^{-1}\prod_{k=0}^n{ O_{T_k}O_{R_k}O_{S_k}}
$$

As complicated as this looks, keep in mind the computer only has to do this once for each object. As such, this ends up being incredibly efficient, as for each vertex, the only math we have to do is $Y=MX$.