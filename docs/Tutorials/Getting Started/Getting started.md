# Introduction

At it's core, blender is a 3D modelling application. This means its first and primary focus is the creation and the editing of 3D models. However, this doesn't mean that this is all that can be done with this piece of software. The following list tries to provide a somewhat exhaustive list of doable tasks in blender, with a tutorial for each:

- [[Tutorial Modeling|Create and edit 3D models]]
- [[Tutorial Sculpting|Sculpting 3D Models]]
- [[Tutorial Texturing|Texture 3D Models]]
- [[Tutorial Rendering|Render 3D scenes and videos]]
- [[Tutorial Post Processing|Add post processing effects to images and videos]]
- [[Tutorial Video Editing|Video Editing]]
- [[Tutorial VFX|Create VFX (visual effects) for example for films]]
- [[Tutorial Motion Tracking|Motion tracking (tracking position information from a video into a 3D virtual scene, used for instance for VFX)]]
- [[Tutorial Grease Pencil|2D Drawing (with the possibility to integrate it in a 3D context)]]
- [[Tutorial Animation|Animate 3D models such as characters]]
- [[Tutorial Simulations|Create 3D simulations (smoke, fire, liquids, hair, cloth, rigid-bodies (3d objects that cannot be deformed) and soft-bodies (3D objects that can be deformed))]]
- [[Tutorial Scripting|Scripting (advanced - used to extend the functionality of the package)]]

In this short tutorial, we will give some basic information on how the application is structured and a aims to bring a broad (but shallow) understanding of how the application functions, before going into more details in individual subjects like modelling or sculpting.
# The application's layout

When opening up blender for the first time, you should be greeted by the following screen.
![[Blender Default view.png]]
Lets break things down:
- The main chunk of the screen is dedicated to the [[3D Viewport]], which is a fancy word to say the 3D scene. In here, we can look around the scene, select [[Objects|objects]], edit them, and visualise the final image. This is were you will most likely spend the majority of your time.
- Above the [[3D Viewport]] lies the navigation bar. The navigation bar is where you can save and load files, edit the preferences for the application,  change the current [[Tabs|tab]], the current [[Scenes|scene]] and the current view layer, amongst others. Essentially, what is present in this tab is everything that has a "global impact" on the project.
- To the top right of the screen, we have the [[Outliner|outliner]]. Think of the outliner as the file hierarchy of your [[Scenes|scene]]. In here you can view your [[Objects|objects]] and your [[Collections|collections]], and their parent-child relationships.
- Right below this is the [[Proprieties|proprieties]] panels. In here, we can find and modify information about the [[Scenes|scene]], the renderer, and the [[Objects|objects]] we are manipulating. This is where you will find most of the parameters you may want to tweak, and more often than not, when you are not working in the [[3D Viewport]], you will be navigating items here. Items here are organised in different tabs displayed to it's left, and parameters are typically found where it is most logical: if you seek to modify a [[Material|material]], head to the material tab. To modify the renderer, head to the render tab, and so on.
- At the complete bottom of the screen is where you will find the [[Timeline|timeline]]. this is used for animation and videos. This can be safely forgotten for the time being.
## Sidenote on [[Tabs|tabs]]

The way blender handles the UI is through the use of [[Panels|panels]]. Each section previously touched upon is one such instance of a [[Panels|panel]], but is by no means an exhaustive list of them all. Truth is, more often than not, we don't need many of them. This is why by default they are all hidden away from us. We can modify the ui to add and remove these [[Panels|panels]], but this is long and tedious.
Instead, blender provides a list of pre-made [[tabs]] which correspond to certain "UI presets". These are available on the top of the screen in the navigation bar. By default, we are placed in the "Layout" panel, which corresponds to the most basic view for blender. Click on any of the other tabs to see how it changes the UI. Once this is done, head back to Layout, as the rest of the tutorial assumes you will be in this [[Tabs|tab]].
# Navigating the view

In order to be efficient in blender, it is first important to know how to look around in the [[3D Viewport]]. This is done through the use of 3 primary motions: [[Navigating the view#Orbiting|orbiting]], [[Navigating the view#Panning|panning]] and [[Navigating the view#Zooming|zooming]]. There are some more advanced ways to navigate the view, the [[Navigating the view]] page provides more information on theses methods.

To [[Navigating the view#Orbiting|orbit]] the camera, click the middle mouse button and drag the mouse around. [[Navigating the view#Panning|Panning]] and [[Navigating the view#Zooming|zooming]] is done the same way, whilst holding down shift for the former and control for the latter.

# Blender as a modal editor

Blender is a modal editor. This means it has a set of [[Modes|modes]] it can operate in. By default the mode that is set is [[Object Mode]]. In this mode, we are able to select different objects and perform object-related operations, such as moving them around, rotating them, creating objects, duplicating objects, destroying objects etc... The current mode is visible on the top left of the viewport.
![[Blender Object Mode location.png]]

>[!warning] The current mode 
>It is _very_ important to know which mode you are in at any time. Depending on the mode, you will not be able to perform the same actions, and given that
>1) it is very easy to change mode (inadvertently pressing the tab key for instance)
>2) There are oftentimes only subtle visual differences describing which mode we are currently in
> 
> This becomes a very common pitfall for new blender users. (even more experienced users have to be careful from time to time) As such: Always verify which mode you are currently in!

To change [[Modes|mode]], you can select the drop down and select the [[Modes|mode]] you wish to tab to. Depending on the current object selection, different [[Modes|modes]] will be displayed: It wouldn't make sense to be in [[Texture Paint Mode (Mesh)|Texture Paint mode]] when manipulating a [[Cameras|camera]]! More information on each [[Modes|mode]] will be available in [[Tutorials|tutorials]] dedicated to this aspect of the program.

# Object creation and deletion

**_When in [[Object Mode]]_**, we can create a new [[Objects|object]] by heading to the `Add` tab to the right of the [[Object Mode]] and selecting the kind of [[Objects|object]] we wish to create. [[Objects]] are grouped by type, so [[Meshes|meshes]] are all going to be under the same name, same for [[Curves|curves]], [[Cameras|cameras]], [[Lights|lights]] and so on.

![[Getting started_Adding an Object.png]]

Alternatively, we can use `shift A` to bring up a popup menu with identical options. To destroy an object, [[Getting started#Object selection|select]] it and press `X`, and confirm the deletion when prompted. The option exists in the menus, under `Object/delete` but no-one uses these drop downs to create or delete [[Objects|objects]], as they are far slower than using shortcuts and they are frequent enough to be worth learning.

Duplicating a [[Getting started#Object selection|selection]] of [[Objects|objects]] can be achieved using the `shift D` shortcut or by navigating to `Object/Duplicate Objects` (again, it is worth investing time in learning the shortcuts) When you duplicate an [[Objects|object]], you are able to move it around using the mouse. Once you are happy with it's position, you can press `left mouse button`. If you wish the duplicated object to stay at the same position as its source, press `right mouse button` or `escape`.

# Object selection

Selecting an [[Objects|object]] is achieved by simply left clicking on the [[Objects|object]] whilst in [[Object Mode]]. To select multiple [[Objects|objects]], hold down `shift` and select each [[Objects|object]] one after the other. Once you have multiple [[Objects|objects]] selected, you may notice one [[Objects|object]] stands out amongst the rest.

![[Active and Passive selections.png]]

In the above example, `Cube` is somehow brighter than `Cube.002`, `Cube.004`, `Cube.007` and `Cube.008`, although they are all selected. This is because `Cube` is _actively_ selected unlike the others who are _passively_ selected. Why does it matter? Oftentimes when dealing with group selections, we want to perform an operation that may need to specify a singular "special" [[Objects|object]]. Here is an example: if you wanted to join all selected [[Objects|objects]] into a single [[Objects|object]], we need to know which [[Objects|object]] to keep. In the example up above, when we join all _selected_ [[Objects|objects]] only `Cube` will remain. If instead, we actively selected `Cube.004`, it would be the only remaining [[Objects|object]] (amongst the selected [[Objects|objects]], `Cube.003`, `Cube.005`, `Cube.006` and `Cube.009` would still be around). From now on, we will refer to the actively selected [[Objects|object]] as "active" and the complete set of selected [[Objects|objects]] as the "selection". Now, whenever you see "Selection to active" appear someplace in blender, you will know what it means.

**By default, the last selected object is the active object**.

# Transforms and Object transformation

Every object in blender has a transform. A transform is comprised of a translation, a rotation and a scale. These are encoded relative to the parent object's own transform. As such, they are encoded as the "Local Transform". To figure out how to display an object onto the screen, blender has to lookup the transform of the current object, as well as it's parent's, it's parent's parent's and so on and so forth. Once it has this list of all of these transforms, it is able to combine them into a new "Global" transform. For the nerds reading this, [[Tutorial Transforms and the maths behind 3D graphics]] gives more information about how this all works.

To move, rotate and scale an object in blender, you can navigate to the left of the screen and select the appropriate option.

![[Gizmos.png]]

In the above image, we can see the gizmos for moving, rotating and scaling as the first 3 options of the lower grouping of options. If you do not see this option, press `T` whilst having the mouse over the viewport. Each one of theses options (either the three mentioned here or any of the others you can see) will alter the way we interact with the world when using the mouse buttons. Hover over any of the options to get a good understanding of what it does. When any of the three transformative options are selected, you will see gizmos appear at the barycentre of all currently selected objects (by default, this can be changed by changing the transform pivot point) so long as there is at least one selected object. Using these gizmos to transform an object is fairly intuitive.

## Hotkeys

This is great, but very slow in practice. It is not uncommon to chain translations, rotations and scales in quick succession, and as such it is far more efficient to use the hotkeys. To move, simply press `G` (for grab), to rotate, `R` (for rotate) and to scale `S` (for scale). Once this is done, simply move the mouse around to perform the transformation. Once you are happy about the new position, simply left click. If you decide to stop the process, simply right click (or press escape).

By default, translations are done tangeantly to the camera, rotation's axis is the vector going from the camera to the objects and scaling is done uniformly. What we can do to remedy this is to press `X`, `Y` or `Z` to specify the translation, rotation or scaling axis after having pressed `G`, `R` or `S`. We can also use `shift X`, `shift Y` or `shift Z` to do the same thing, except instead of having x, y and z be the only effected axis, they become the only _unaffected_ axis. For example, to move on the x, y plane, simply press `G` then `shift Z`.

Still after pressing any of these three shortcuts, we can press `shift` to enter _precision mode_. In this mode, mouse sensitivity is reduced to allow for more precise movements. For even more precise movements, you can press control to enable snapping.

![[Object Transform.png]]