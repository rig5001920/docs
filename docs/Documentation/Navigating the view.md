Navigation in Blender's [[3D Viewport]] is done through the use of 3 primary motions: [[Navigating the view#Orbiting|orbiting]], [[Navigating the view#Panning|panning]] and [[Navigating the view#Zooming|zooming]]. However, there are some more advanced ways to navigate the view, such as to [[Navigating the view#Focusing|focus]] on a particular object.

## Orbiting

Orbiting the view corresponds to the act of rotating the camera around a point in space called the [[Navigating the view#The focus point|focus point]]. To orbit around, **_click the middle mouse button[^1] and drag the mouse_**.
## Panning

Panning corresponds to shifting the position of the camera tangentially to the direction which it's facing. For example, if the camera is facing forward, the act of panning results in a left-right up-down shift in position for the camera. This will modify the camera's [[Navigating the view#The focus point|focus point]]. In order to pan, **_hold down the shift button, click the middle mouse button[^1] and drag the mouse_**.

## Zooming

Zooming is self explanatory: this brings the camera closer or farther to its [[Navigating the view#The focus point|focus point]]. In order to zoom, **_hold down control, click the middle mouse button[^1] and drag the mouse_**.

# Focusing

Focusing on an object is an efficient way to quickly move the camera to said object. It changes the camera's [[Navigating the view#The focus point|focus point]] to the objects's [[Object Origin|origin]] and [[Navigating the view#Zooming|zooms]] the camera to match the scale of the object. In order to focus on an object, select the object and **_go to `view/Frame Selected` in the top left of the viewport_**. In addition to this, if you have a numpad you can use the `numpad .` button as a shortcut. **_You can also go to `view/Frame All` to focus on all objects in the scene_**.

![[Navigating the view_Focusing.png]]
## The focus point

The focus point is a point in the 3D view used for [[Navigating the view#Orbiting|orbiting]]. In short, it is the point around which we rotate. This point is by default the origin of the 3D view, but can be modified as time goes on, for instance by [[Navigating the view#Panning|panning]] or by [[Navigating the view#Focusing|focusing]] on an object.

[^1]: Note: when using a apple mouse or a apple trackpad, there is no need to press down on the middle mouse button, as they are tactile