In blender, whenever we want to add something to our scene, we do so with the aid of an object. We can draw an analogy with filesystems, where objects correspond to files and [[Collections|collections]] correspond to folders[^1].

## Types of objects

Blender allows us to create a broad range of different objects who don't always work in the same way. As such, they have been categorised into specialised types of objects, an exhaustive list of which is provided below:
### Basic objects
- [[Meshes]]: An instance of a 3D model.
- [[Lights]]: A light used for rendering or to embed in a file capable of storing an entire 3D scene (.fbx, .dae, .usd) that can then be exported into another application capable of 3D rendering such as a game engine.
- [[Empties]]: An empty object. This object cannot be rendered, but can be used for visualisation or for [[Constraints|contraints]].
- [[Text]]: A 3D representation of text.
- [[Curves]]: A 3D Parametric curve.
- [[Cameras]]: A 3D Camera used for rendering.

### Advanced objects
- [[Armatures]]: A collection of bones used for animating characters or other moving objects
- [[Volume]]: A cloud of values in 3D space used for instance for smoke and fire.
- [[Grease Pencil]]: A powerful drawing tool in 3D
- [[Lattice]]: A lattice of vertices used for mesh deformation.
- [[Surfaces]]: A 3D parametric surface used for hard-surface modelling.
- [[Metaballs]]: See signed distance fields for more information
- [[Light Probes]]: Used for caching reflections (in [[EEVEE]])
- [[Speakers]]: An audio source
- [[Force Fields]]: A object used to create a force field useful for physics simulation (for example to simulate wind)
- [[Collection instances]]: An object referencing the content of a [[Collections|collection]] in order to instance it into the scene without duplicating the content's data.

[^1]: This is a big simplification, as in practice it is possible (and very useful!) to nest objects in other objects. As such, they are able to behave as folders with the only difference being folders are unable to hold any other information other than its list of children. [[Collections]] are more folder-like in this regard (although still more powerful in some regards, see the [[Collections|collections]] page for more information)