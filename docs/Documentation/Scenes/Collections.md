A collection loosely corresponds to a folder in a classic file system. They are used to structure a blender file. Unlike folders however, collections contains information based off of the view layer.

# Creating a collection

To create a collection, head to the [[Outliner|outliner]], right click on the soon-to-be parent of the collection we want to create and click on `New`.
![[Collections_Creation.png]]

# Restrictions

Much like [[Objects|objects]], collections can have restrictions applied to them. These include including/excluding the collection from the view layer (ie, enabling or disabling the collection), toggling collection selectability (the ability for the collection to be selected), visibility, viewport visibility, render visibility, holdout (will not be rendered) and indirect only (will only indirectly contribute to the render). These last two are more advanced and we recommend reading the blender docs for more information.

By default, not all restrictions are visible in the [[Outliner|outlier]] as to not over-clutter the view, those that are are displayed to the right of the collection **and act as a toggle**. To change which are visible and which are hidden, select the filter icon to the right of the search bar in the [[Outliner|outliner]] and enable or disable the restrictions we wish to see for our collections and our [[Objects|objects]]. Visible restrictions are coloured in blue (in the default blender theme) and invisible restrictions are greyed out.

![[Restrictions.png]]

# Colouring

We can colour a collection by right clicking on the collection in the [[Outliner|outliner]], heading down to the colour tag and selecting the colour we wish to display the collection by.

![[Collections_Colouring.png]]