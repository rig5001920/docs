# docs

This repo contains an obsidian vault with tutorials
on how to use Blender as well as general 3d modeling
information intended for students of the Telecom
SudParis school in France who wish to partake in the school's
3D Modeling club. It is maintained by the board of said club.
